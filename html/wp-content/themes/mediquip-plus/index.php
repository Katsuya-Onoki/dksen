<?php
/**
 * The template for displaying home page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Mediquip Plus
 */

get_header(); 
?>
<div class="container">
     <div class="contentsecwrap">
        <section class="site-main <?php if ( !is_active_sidebar( 'sidebar-1' ) ) { ?>fullwidth<?php } ?>">
        	 <div class="postsec-list">
                <h1>電験三種とは</h1>
                    <p>「電験」とは「電気主任技術者試験」の略称です。<br>電気主任技術者には一種から三種まであり、電験三種とは第三種電気主任技術者になるための資格のことを言います。</p>
                <!-- <h2>おすすめテキスト</h2> -->
                    
                    <div id="index_recommend_ranking" class="clearfix">
    <h2 class="g_design_headline rich_font">おすすめテキスト</h2>
    <div id="ranking_list">
              <article class="item clearfix">
                <div class="data_top clearfix">
                  <a class="imagew" href="https://bookstore.tac-school.co.jp/book/detail/07417/" target="blank" title="みんなが欲しかった電験三種の教科書" rel="nofollow"><img src="/img/common/load.gif" data-layzr="https://storage.googleapis.com/storage.takkenshi.jp/img/bk/book/15.jpg" class="attachment-size2 size-size2 g-post-image" alt="" sizes="(max-width: 160px) 100vw, 200px"/></a>
                  <div class="data clearfix">
                    <a class="title" href="https://bookstore.tac-school.co.jp/book/detail/07417/" target="blank" title="みんなが欲しかった電験三種の教科書" rel="nofollow">みんなが欲しかった電験三種の教科書</a>
                    <ul class="meta">
                      <li>定価 3,240円(税込)</li>
                      <li class="kai-date">会員価額 2,916円(税込)</li>
                    </ul>
                    <p class="catch">魔法のようによくわかる教科書です。フルカラーで丁寧に解説されているので、法律学習に苦手意識を持っている人にオススメです。</p>
                    <div class="button">
                      <a class="g_design_button text_top_button" href="
                          /宅建士テキスト/
                                                    ">詳細を見る</a>
                    </div>
                  </div>
                </div>
              </article>
          <article class="item clearfix">
            <div class="data_top clearfix">
              <a class="imagew" href="https://bookstore.tac-school.co.jp/book/detail/61621/" target="blank" title="みんなが欲しかった！電験三種 基礎学習セット" rel="nofollow"><img src="/img/common/load.gif" data-layzr="https://storage.googleapis.com/storage.takkenshi.jp/img/bk/set/2.jpg" class="attachment-size2 size-size2 g-post-image" alt="" sizes="(max-width: 160px) 100vw, 200px"/></a>
              <div class="data clearfix">
                <a class="title" href="https://bookstore.tac-school.co.jp/book/detail/61621/" target="blank" title="みんなが欲しかった！電験三種 基礎学習セット" rel="nofollow">みんなが欲しかった！電験三種 基礎学習セット</a>
                <ul class="meta">
                  <li>定価 5,940円(税込)</li>
                  <li class="kai-date">会員価額 5,049円(税込)</li>
                </ul>
                <p class="catch">勉強の質と効率が劇的に上がる仕組みが満載の基本学習セットです。</p>
                <div class="button">
                  <a class="g_design_button text_top_button" href="/宅建士テキスト/">詳細を見る</a>
                </div>
              </div>
            </div>
          </article>
          <article class="item clearfix">
            <div class="data_top clearfix">
              <a class="imagew" href="https://bookstore.tac-school.co.jp/book/detail/43890/" target="blank" title="独学道場【みんなが欲しかった】フルパック" rel="nofollow"><img src="/img/common/load.gif" data-layzr="https://storage.googleapis.com/storage.takkenshi.jp/img/bk/course/1.jpg" class="attachment-size2 size-size2 g-post-image" alt="" sizes="(max-width: 160px) 100vw, 200px"/></a>
              <div class="data clearfix">
                <a class="title" href="https://bookstore.tac-school.co.jp/book/detail/43890/" target="blank" title="独学道場【みんなが欲しかった】フルパック" rel="nofollow">独学道場【みんなが欲しかった】フルパック</a>
                <ul class="meta">
                  <li class="kai-date">価額 29,800円(税込)</li>
                </ul>
                <p class="catch">“人気No1書籍が合格へナビゲート!”法律系試験に初挑戦の方にもおススメのコース!</p>
                <div class="button">
                  <a class="g_design_button text_top_button" href="/宅建士通信講座/">詳細を見る</a>
                </div>
              </div>
            </div>
          </article>
      <article class="item clearfix">
        <div class="data_top clearfix">
          <a class="imagew" href="https://itunes.apple.com/jp/app/id1187979884?mt=8" target="blank" title="宅建士過去問スマホアプリ"><img src="/img/common/load.gif" data-layzr="/img/app/kado_app_icon.jpg" class="attachment-size2 size-size2 g-post-image" alt="" sizes="(max-width: 160px) 100vw, 200px"/></a>
          <div class="data clearfix">
            <a class="title" href="https://itunes.apple.com/jp/app/id1187979884?mt=8" target="blank" title="電験三種過去問スマホアプリ">電験三種過去問スマホアプリ</a>
            <p class="catch">12年分過去問が解ける！「みんなが欲しかった電験三種の12年過去問題集」と連動したアプリです！</p>
            <div class="button">
              <a class="g_design_button text_top_button" href="https://itunes.apple.com/jp/app/id1187979884?mt=8">詳細を見る</a>
            </div>
          </div>
        </div>
      </article>
    </div>
  </div>

					<!-- <?php
                    if ( have_posts() ) :
                        // Start the Loop.
                        while ( have_posts() ) : the_post();
                            /*
                             * Include the post format-specific template for the content. If you want to
                             * use this in a child theme, then include a file called called content-___.php
                             * (where ___ is the post format) and that will be used instead.
                             */
                            get_template_part( 'content' );
                    
                        endwhile;
                        // Previous/next post navigation.
                        the_posts_pagination();
                    
                    else :
                        // If no content, include the "No posts found" template.
                         get_template_part( 'no-results', 'index' );
                    
                    endif;
                    ?> -->



                    </div><!-- postsec-list -->
             </section>
      
        <?php get_sidebar();?>     
        <div class="clear"></div>
    </div><!-- site-aligner -->
</div><!-- content -->
<?php get_footer(); ?>